﻿using Foundation;
using UIKit;
using PanCardView.iOS;
using FFImageLoading.Forms.Platform;
using Lottie.Forms.iOS.Renderers;

namespace PanCardViewSample.iOS
{
	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
			CachedImageRenderer.Init();
            CardsViewRenderer.Preserve();

			global::Xamarin.Forms.Forms.Init();
            AnimationViewRenderer.Init();

            LoadApplication(new App());

			return base.FinishedLaunching(app, options);
		}
	}
}