﻿using PanCardViewSample.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PanCardViewSample.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CardViewScroll : ContentPage
    {
        public CardViewScroll()
        {
            InitializeComponent();

            BindingContext = new CardsSampleViewModel();
        }
    }
}