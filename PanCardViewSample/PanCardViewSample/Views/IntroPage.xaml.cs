﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PanCardViewSample.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IntroPage : ContentPage
    {
        public ObservableCollection<object> Items { get; }

        public IntroPage()
        {
            InitializeComponent();

            Items = new ObservableCollection<object>
            {
                new { Text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut magna malesuada, convallis elit quis." },
                new { Text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut magna malesuada, convallis elit quis." },
                new { Text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut magna malesuada, convallis elit quis." },
            };
           
            BindingContext = this;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            var animation = new Animation(v => AnimationView.Progress = (float)v, 0, 0.34);
            animation.Commit(AnimationView, "toTheMoon", 16, 2000, Easing.Linear);
        }

        private void CarouselView_ItemSwiped(PanCardView.CardsView view, PanCardView.EventArgs.ItemSwipedEventArgs args)
        {
            if (args.Direction == PanCardView.Enums.SwipeDirection.Right)
            {
                var animation = new Animation(v => AnimationView.Progress = (float)v, AnimationView.Progress, Math.Max(0, AnimationView.Progress - 0.34));
                animation.Commit(AnimationView, "toTheMoon", 16, 2000, Easing.Linear);
            }
            else if (args.Direction == PanCardView.Enums.SwipeDirection.Left)
            {
                var animation = new Animation(v => AnimationView.Progress = (float)v, AnimationView.Progress, Math.Min(1, AnimationView.Progress + 0.34));
                animation.Commit(AnimationView, "toTheMoon", 16, 2000, Easing.Linear);
            }
        }
    }
}