﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PanCardViewSample.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ShorlistView : ContentPage
	{
		public ShorlistView (IList<View> children)
		{
            InitializeComponent();

            foreach (var child in children)
            {
                ListLayout.Children.Add(child);
            }
		}
	}
}