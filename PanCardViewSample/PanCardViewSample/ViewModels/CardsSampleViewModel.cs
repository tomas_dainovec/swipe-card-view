﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;
using System.Linq;
using FFImageLoading;
using PanCardView.Extensions;
using PanCardView.EventArgs;
using PanCardViewSample.Views;
using System;
using PanCardView.Enums;

namespace PanCardViewSample.ViewModels
{
	public sealed class CardsSampleViewModel : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		private int _currentIndex;
		private int _ImageCount = 500;

		public CardsSampleViewModel()
		{
			Items = new ObservableCollection<object>
			{
				new { Source = CreateSource(), Text = "Slide 1", Ind = _ImageCount++, Color = Color.Red },
				new { Source = CreateSource(), Text = "Slide 2", Ind = _ImageCount++, Color = Color.Green },
				new { Source = CreateSource(), Text = "Slide 3", Ind = _ImageCount++, Color = Color.Gold },
				new { Source = CreateSource(), Text = "Slide 4", Ind = _ImageCount++, Color = Color.Silver },
				new { Source = CreateSource(), Text = "Slide 5", Ind = _ImageCount++, Color = Color.Blue },
                new { Color = Color.WhiteSmoke }
            };

            ShortList = new ObservableCollection<View>();

            PanPositionChangedCommand = new Command(v =>
			{
				var val = (bool)v;
				if (val)
				{
					CurrentIndex += 1;
					return;
				}

				CurrentIndex -= 1;
			});

			RemoveCurrentItemCommand = new Command(() =>
			{
				if (!Items.Any())
				{ 
					return;
				}
				Items.RemoveAt(CurrentIndex.ToCyclingIndex(Items.Count));
			});

            SwippedCommand = new Command<ItemSwipedEventArgs>((args) => {
                if (args.Direction == PanCardView.Enums.SwipeDirection.Left)
                {
                    
                }
                else if (args.Direction == PanCardView.Enums.SwipeDirection.Right)
                {
                    System.Type type = args.Item.GetType();
                    var text = (string)type.GetProperty("Text").GetValue(args.Item, null);
                    ShortList.Add(new Label { Text = text });
                }

                if (args.Index == Items.Count - 2)
                {
                    IsSwipeEnabled = false;
                }

                IsCrossVisible = false;
                IsCheckVisible = false;
            });

            ViewShortlistCommand = new Command(async () => {
                await App.Current.MainPage.Navigation.PushAsync(new ShorlistView(ShortList));
            });

            UserInteractedCommand = new Command<UserInteractedEventArgs>((args) => {
                switch(args.Status)
                {
                    case UserInteractionStatus.Running:
                        IsCrossVisible = args.Diff < 0;
                        IsCheckVisible = args.Diff > 0;
                        break;
                    case UserInteractionStatus.Ended:
                        IsCheckVisible = false;
                        IsCrossVisible = false;
                        break;
                }
            });
        }

		public ICommand PanPositionChangedCommand { get; }

		public ICommand RemoveCurrentItemCommand { get; }

        public ICommand SwippedCommand { get; }

        public ICommand ViewShortlistCommand { get; }

        public ICommand UserInteractedCommand { get; }

        private bool _isSwipeEnabled = true;
        

        public bool IsSwipeEnabled
        {
            get => _isSwipeEnabled;
            set {
                if (_isSwipeEnabled != value)
                {
                    _isSwipeEnabled = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsSwipeEnabled)));
                }
            }
        }

		public int CurrentIndex
		{
			get => _currentIndex;
			set
			{
				_currentIndex = value;
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(CurrentIndex)));
			}
		}

        private bool _isCheckVisible;
        public bool IsCheckVisible
        {
            get => _isCheckVisible;
            set
            {
                if (_isCheckVisible != value)
                {
                    _isCheckVisible = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsCheckVisible)));
                }
            }
        }

        private bool _isCrossVisible;
        public bool IsCrossVisible
        {
            get => _isCrossVisible;
            set
            {
                if (_isCrossVisible != value)
                {
                    _isCrossVisible = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsCrossVisible)));
                }
            }
        }


        public ObservableCollection<object> Items { get; }

        public ObservableCollection<View> ShortList { get; }

        private string CreateSource()
		{
			var source = $"https://picsum.photos/500/500?image={_ImageCount}";
			return source;
		}
	}
}